//Use Inplace Reverse and Reverse The Array Of Characters

import java.io.*;
class Demo{
	String Reverse(char arr[]){
		
		int i =0;
		while(i<arr.length/2){
			char temp = arr[i];
			arr[i]=arr[arr.length-i-1];
			arr[arr.length-i-1] = temp;
			i++;
		}

		return new String(arr);
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Entyer The String");
		char arr[] = br.readLine().toCharArray();
		
		Demo obj = new Demo();

		String rev = obj.Reverse(arr);
		System.out.println(rev);
	}
}
