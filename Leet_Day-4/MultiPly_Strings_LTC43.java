//Multiply The Two Strings and The Result is also Represented In Struing Format

import java.io.*;
class Demo{
	static String MultiplyStrings(String num1,String num2){
		int a = Integer.parseInt(num1);
		int b = Integer.parseInt(num2);
		return Integer.toString(a*b);
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number1");
		String num1 = br.readLine();
		System.out.println("Enter The Number2");
		String num2 = br.readLine();

		String ret = MultiplyStrings(num1,num2);
		System.out.println(ret.toString());
		
	}
}
